README
------
The files in the nl directory can be used to configure a Solr core with support for the Dutch language.
When using the search_api_solr_mutlingual module for Drupal 8 you should already have
a blank version of those files available.


SOURCES
-------

synonyms_nl.txt: https://github.com/wizzlern/dutch-solr/blob/master/irregular_verbs_nl.txt
nouns_nl.txt: https://github.com/nielsbom/Hangman/blob/master/dutch_org.txt

Other files are generated using the search_api_solr_multilingual module.
